Ce TP a pour but de mettre en place :
 - Prometheus et Grafana 
 - Un node exporter ( que l'on va monitorer)

# Lancement du projet :
`docker-compose up -d`

# Grafana :
IHM : <IP:3000>
*user_default=admin*
*passwd_default=admin*

# Prometeheus :
IHM : <IP:9090>
